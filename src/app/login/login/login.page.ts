import {Component, OnInit} from '@angular/core';
import {AppServiceService} from '../../services/app-service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastController} from '@ionic/angular';
import {faSignInAlt} from '@fortawesome/free-solid-svg-icons';
import {ResponseModels} from '../../services/response-models';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    faSign = faSignInAlt;
    form: FormGroup;
    loading = false;
    sSubmit = false;
    response: ResponseModels ;

    constructor(private toastController: ToastController, private service: AppServiceService, private fb: FormBuilder, private router: Router) {

        this.form = this.fb.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]]
        });
    }

    ngOnInit() {

    }

    login() {
        this.sSubmit = true;
        if (this.form.valid) {
            this.loading = true;
            this.service.authUser(this.form.value).subscribe(
                res => {
                    this.response = res ;
                    if (this.response.status) {
                        console.log(this.response.data[0].token);
                        this.service.storeUserId(this.response.data[0].token, this.response.data[0].Name);
                        console.log(localStorage.getItem('token'));
                        this.router.navigate(['/home']);
                        this.loading = false;
                    } else {
                        this.loading = false;
                        this.presentToast('اطلاعات وارده اشتباه هست');
                    }
                },
                err => {
                    this.loading = false;
                    this.presentToast('خطای ارتباط با سرور');

                }
            );
        }
    }

    async presentToast(message) {
        const toast = await this.toastController.create({
            message,
            duration: 2000,
            cssClass: 'secondary'
        });
        await toast.present();
    }
}
