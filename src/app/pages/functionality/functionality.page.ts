import {Component, OnInit} from '@angular/core';
import {FunctinalityModel} from '../../services/functinality-model';
import {FilterModel} from '../../services/filter-model';
import {ResponseModels} from '../../services/response-models';
import {ToastService} from '../../services/toast.service';
import {AppServiceService} from '../../services/app-service.service';


@Component({
    selector: 'app-functionality',
    templateUrl: './functionality.page.html',
    styleUrls: ['./functionality.page.scss'],
})
export class FunctionalityPage implements OnInit {
    loading = false ;

    dataFromModels: FunctinalityModel[] = [];


    filterActive = false;
    unitName: string;
    title = 'گزارشات عملکرد';
    result: ResponseModels ;
    getTodayDate = new Date();
    data = {
        dateFrom: this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        dateUntil:  this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        organization: null ,
        organizationUnit: null ,
        systemUserId: null ,
        workShift: null
    };

    constructor(private toast: ToastService , private service: AppServiceService) {

    }

    ngOnInit() {
        this.getData(this.data);
    }

    getData(dataModel: FilterModel){
        this.loading = true ;
        this.service.getDebtor(dataModel).subscribe(
            res => {
                if (res.status) {
                    this.result = res ;
                    //console.log(this.result.data);
                    this.dataFromModels = this.result.data ;
                    this.loading = false ;
                    if (this.dataFromModels.length === 0){
                        this.toast.presentToast('اطلاعاتی ثبت نشده').then();
                    }
                }
            },
            err => {
                this.toast.presentToast('خطای ارتباط با سرور').then();
                console.log(err);
                this.loading = false ;

            }
        );
    }
    dataFromModel(dataFromFilter: FilterModel) {
        this.getData(dataFromFilter);
    }



}
