import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {FunctionalityPage} from './functionality.page';
import {HeaderComponent} from '../header/header.component';
import {FilterModule} from '../../filter/filter.module';

const routs = [
    {
        path: '',
        component: FunctionalityPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routs),
        FilterModule,
    ],
    exports: [
        HeaderComponent
    ],
    declarations: [FunctionalityPage]
})
export class FunctionalityModule {
}
