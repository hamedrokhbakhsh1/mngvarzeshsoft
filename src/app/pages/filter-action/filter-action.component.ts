import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AlertController, PickerController} from '@ionic/angular';
import {FilterModel} from '../../services/filter-model';
import {FilterDataService} from '../../services/filter-data.service';
import * as moment from 'jalali-moment';
moment.locale('fa');


@Component({
  selector: 'app-filter-action',
  templateUrl: './filter-action.component.html',
  styleUrls: ['./filter-action.component.scss'],
})
export class FilterActionComponent implements OnInit {


  dateFrom: any = {};
  order: any = {
    dateFrom: ''
  };
  dateUntil: any = {};
  orderUntil: any = {
    dateUntil: '' 
  };

   @Input() loading = false;
    showFilter = false ;
  shiftName = 'بدون انتخاب';
  workShifts = {data: []};
  userData = {data: []};
  organizations = { data: []};
  organizationUnits = { data: []};
  getTodayDate = new Date();
  data: FilterModel = {
    dateFrom: this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
    dateUntil:  this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
    organization: null ,
    organizationUnit: null ,
    systemUserId: null ,
    workShift: null
  };

  @Output() open: EventEmitter<any> = new EventEmitter();
  userName = 'بدون انتخاب';
  organizationName = 'بدون انتخاب';
  organizationUnitName = 'بدون انتخاب';
  m = moment().locale('fa').format('YYYY/MM/DD');
  displayDateUntil = this.m;
  displayDateFrom = this.m ;
  constructor(public alertController: AlertController , public filterService: FilterDataService , private pickerController: PickerController) {
  }
  initDatePickerData() {
    if (!this.dateFrom) {
      this.dateFrom = {};
    }
    if (!this.dateUntil) {
      this.dateUntil = {};
    }
    this.dateFrom.dayOptions = this.getDays();
    this.dateFrom.monthOptions = this.getMonths();
    this.dateFrom.yearsOptions = this.getYears();
    this.dateUntil.dayOptions = this.getDays();
    this.dateUntil.monthOptions = this.getMonths();
    this.dateUntil.yearsOptions = this.getYears();
    this.selectDefaultDate();
  }


  ngOnInit() {
    this.loading = true ;
    if (this.filterService.getWorkShiftData()){
      if (!this.workShifts.data.length){
        this.workShifts = this.filterService.getWorkShiftData();
      }
    }
    if (this.filterService.getUserData()){
      if (!this.userData.data.length){
        this.userData = this.filterService.getUserData() ;
      }

    }
    if (this.filterService.getOrganizationData()){
      if (!this.organizations.data.length){
        this.organizations = this.filterService.getOrganizationData() ;
      }
    }

    if (this.filterService.getOrganizationUnitData()){
      if (!this.organizationUnits.data.length){
        this.organizationUnits = this.filterService.getOrganizationUnitData() ;
      }

    }
    this.loading = false ;
  }

  filter() {
    this.showFilter = ! this.showFilter ;
  }

  async selectShift() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'مجموعه ها',
      inputs: this.workShifts.data,
      buttons: [
        {
          text: 'لغو',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ثبت',
          handler: (e) => {
            this.data.workShift = e.ID ;
            this.shiftName = e.Name;
          }
        }
      ]
    });

    await alert.present();
  }

  async selectUser() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'افراد مجموعه',
      inputs: this.userData.data,
      buttons: [
        {
          text: 'لغو',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ثبت',
          handler: (e) => {
            this.data.systemUserId = e.ID;
            this.userName = e.DisplayName;
          }
        }
      ]
    });

    await alert.present();
  }
  filterAction() {
    console.log(this.data)
    this.open.emit(this.data);
    this.loading = true ;
    this.showFilter = ! this.showFilter ;
  }

  async selectOrganization() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: ' سازمان ها',
      inputs: this.organizations.data,
      buttons: [
        {
          text: 'لغو',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ثبت',
          handler: (e) => {
            this.organizationName = e.Title ;
            this.data.organization = e.ID ;
          }
        }
      ]
    });

    await alert.present();
  }

  async selectOrganizationUnit() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: ' واحد سازمانی',
      inputs: this.organizationUnits.data,
      buttons: [
        {
          text: 'لغو',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ثبت',
          handler: (e) => {
            this.organizationUnitName = e.Name ;
            this.data.organizationUnit = e.ID;
          }
        }
      ]
    });


    await alert.present();
  }

  filterReset() {
    this.data = {
      dateFrom: this.getTodayDate.getFullYear() + '-' + (this.getTodayDate.getMonth() + 1) + '-' + this.getTodayDate.getDate() ,
      dateUntil:  this.getTodayDate.getFullYear() + '-' + (this.getTodayDate.getMonth() + 1) + '-' + this.getTodayDate.getDate() ,
      organization: null ,
      organizationUnit: null ,
      systemUserId: null ,
      workShift: null
    };
    this.organizationUnitName = 'بدون انتخاب';
    this.organizationName =  'بدون انتخاب';
    this.userName =  'بدون انتخاب';
    this.shiftName =  'بدون انتخاب';
    this.displayDateUntil = this.m;
    this.dateFrom.pDate = this.initDatePickerData() ;
    this.dateUntil.pDate = this.initDatePickerData() ;
  }

   async openDatePickerFrom() {
      this.initDatePickerData();
      const picker = await this.pickerController.create({
        mode: 'ios',
        keyboardClose: true,
        buttons: [{
          text: 'انتخاب',
          handler: (value) => {
            this.dateFrom.yearsIndex = this.getYears().findIndex(x => x.value == value.years.value);
            this.dateFrom.years = value.years.value;
            this.dateFrom.month = value.month.value;
            this.dateFrom.day = value.day.value;
            this.initDate();
            console.log(this.order.gDate);
            this.data.dateFrom = this.order.gDate ;
          }
        }, {
          text: 'انصراف',
          role: 'cancel'
        }],
        columns: [
          {
            name: 'day',
            options: this.dateFrom.dayOptions,
            selectedIndex: this.dateFrom.day - 1
          },
          {
            name: 'month',
            options: this.dateFrom.monthOptions,
            selectedIndex: this.dateFrom.month - 1
          },
          {
            name: 'years',
            options: this.dateFrom.yearsOptions,
            selectedIndex: this.dateFrom.yearsIndex
          }
        ]
      });
      await picker.present();
    }


  getDays(): any[] {
    const out: any[] = [];
    for (let i = 1; i <= 31; i++) {
      out.push({
        text: this.setPad(i),
        value: i
      });
    }
    return out;
  }
  getYears() {
    const current = moment().jYear();
    const out: any[] = [];
    for (let i = current-20; i <= current+3 ;i++) {
      out.push({
        text: i,
        value: i
      });
    }
    return out;
  }
  getMonths(): any[] {
    const months = Array.apply(0, Array(12)).map((_, i) => moment().jMonth(i).format('jMMMM'));
    const out: any[] = [];
    for (let i = 1; i <= months.length; i++) {
      out.push({
        text: months[i - 1],
        value: i
      });
    }
    return out;
  }

  setPad(val: number) {
    return (val + '').padStart(2, '0');
  }
  selectDefaultDate() {
    if (!this.dateFrom.day || !this.dateFrom.month || !this.dateFrom.years) {
      const m = moment();
      this.dateFrom.years = m.jYear();
      this.dateFrom.yearsIndex = this.getYears().findIndex(x => x.value == this.dateFrom.years);
      this.dateFrom.month = m.jMonth() + 1;
      this.dateFrom.day = m.jDate();
      this.initDate();
    }
    if (!this.dateUntil.day || !this.dateUntil.month || !this.dateUntil.years) {
      const m = moment();
      this.dateUntil.years = m.jYear();
      this.dateUntil.yearsIndex = this.getYears().findIndex(x => x.value == this.dateUntil.years);
      this.dateUntil.month = m.jMonth() + 1;
      this.dateUntil.day = m.jDate();
      this.initDate();
    }
  }
  initDate() {
    this.dateFrom.pDate = moment(`${this.dateFrom.years}/${this.dateFrom.month}/${this.dateFrom.day}`, 'jYYYY-jM-jD');
    this.order.gDate = this.dateFrom.pDate.locale('en').format('YYYY-MM-DD');;
    this.order.dateFrom = this.dateFrom.pDate.format('jYYYY-jMM-jDD');
  }
  initDateUntil() {
    this.dateUntil.pDate = moment(`${this.dateUntil.years}-${this.dateUntil.month}-${this.dateUntil.day}`, 'jYYYY-jM-jD');
    this.orderUntil.gDate = this.dateUntil.pDate.locale('en').format('YYYY-MM-DD');
    this.orderUntil.dateUntil = this.dateUntil.pDate.format('YYYY-MM-DD');
  }


  async openDatePickerUntil() {
      this.initDatePickerData();
      const picker = await this.pickerController.create({
        mode: 'ios',
        keyboardClose: true,
        buttons: [{
          text: 'انتخاب',
          handler: (value) => {
            this.dateUntil.yearsIndex = this.getYears().findIndex(x => x.value == value.years.value);
            this.dateUntil.years = value.years.value;
            this.dateUntil.month = value.month.value;
            this.dateUntil.day = value.day.value;
            this.initDateUntil();
            this.data.dateUntil = this.orderUntil.gDate ;
          }
        }, {
          text: 'انصراف',
          role: 'cancel'
        }],
        columns: [
          {
            name: 'day',
            options: this.dateUntil.dayOptions,
            selectedIndex: this.dateUntil.day - 1
          },
          {
            name: 'month',
            options: this.dateUntil.monthOptions,
            selectedIndex: this.dateUntil.month - 1
          },
          {
            name: 'years',
            options: this.dateUntil.yearsOptions,
            selectedIndex: this.dateUntil.yearsIndex
          }
        ]
      });
      await picker.present();
    }

}
