import {Component, OnInit} from '@angular/core';
import {AppServiceService} from '../../services/app-service.service';
import {Router} from '@angular/router';
import * as moment from 'jalali-moment';
import {FilterModel} from '../../services/filter-model';
import {ResponseModels} from '../../services/response-models';
import {ToastService} from '../../services/toast.service';



@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    loading = true;
    m = moment().locale('fa').format('YYYY-MM-DD');
    date = moment().locale('fa').format(' dddd,D MMMM YYYY     ');
    presentNumber = 0;
    exitNumber = 0;
    // tslint:disable-next-line:variable-name
    SumTotalAmount_Receipt = 0;
    functionTotalAmount = 0;
    result: ResponseModels ;
    getTodayDate = new Date();
    data = {
        dateFrom: this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        dateUntil:  this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        organization: null ,
        organizationUnit: null ,
        systemUserId: null ,
        workShift: null
    };



    constructor(private service: AppServiceService, private router: Router , private toast: ToastService) {

    }

    ngOnInit() {
        this.getData(this.data);
    }

    reciveReport() {
        this.router.navigate(['/home/recive-report']);
    }

    reciptionSituation() {
        this.router.navigate(['/home/reciption-situation']);
    }

    functionalitiReport() {
        this.router.navigate(['/home/functionality']);
    }

     dataFromModel(dataFromFilter: FilterModel) {
        this.getData(dataFromFilter);
    }

    getData(dataModel: FilterModel){
        this.loading = true ;
        this.service.getAllReport(dataModel).subscribe(
            res => {
                if (res.status) {
                    this.result = res
                    // console.log(this.result.data[0]);
                    this.presentNumber = this.result.data[0].PresentMemberCount;
                    this.exitNumber = this.result.data[0].ExitedMemberCount;
                    this.SumTotalAmount_Receipt = this.result.data[0].SumTotalAmount_Creditor;
                    this.functionTotalAmount = this.result.data[0].SumTotalAmount_Receipt;
                    this.loading = false ;
                }
            },
            err => {
                this.toast.presentToast('خطای ارتباط با سرور').then();
                this.loading = false;
                console.log(err);
            }
        );
    }

}
