import {Component, OnInit} from '@angular/core';
import {AppServiceService} from '../../services/app-service.service';
import {Router} from '@angular/router';
import {FilterModel} from '../../services/filter-model';
import {ToastService} from '../../services/toast.service';
import {ResponseModels} from '../../services/response-models';


@Component({
    selector: 'app-reciption-situation',
    templateUrl: './reciption-situation.page.html',
    styleUrls: ['./reciption-situation.page.scss'],
})
export class ReciptionSituationPage implements OnInit {

    loading = false ;
    getTodayDate = new Date();
    result: ResponseModels ;

    data = {
        dateFrom: this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        dateUntil:  this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        organization: null ,
        organizationUnit: null ,
        systemUserId: null ,
        workShift: null
    };

    constructor(private service: AppServiceService , private router: Router , private toast: ToastService) {

    }

    presentNumber = 0;
    exitNumber = 0;
    all = 0;
    filterActive = false;
    unitName = 'سازمان';
    title = ' وضعیت پذیرش';
    ngOnInit() {
        this.getData(this.data);
    }

    back() {
        this.router.navigate(['/home']).then();
    }

    getData(dataModel: FilterModel){
        this.loading = true ;
        this.service.getReportSituation(dataModel).subscribe(
            res => {
                if (res.status) {
                    this.result = res ;
                    //console.log(this.result.data[0]);
                    this.exitNumber = this.result.data[0].ExitedMemberCount ;
                    this.presentNumber = this.result.data[0].PresentMemberCount ;
                    this.all = this.result.data[0].SumTotalMemberCount ;



                    this.loading = false ;
                }
            },
            err => {
                this.loading = false ;
                this.toast.presentToast('خطای ارتباط با سرور').then();
                console.log(err)
            }
        );
    }
    dataFromModel(dataFromFilter: FilterModel) {
        this.getData(dataFromFilter);
    }
}
