import {Component, OnInit} from '@angular/core';
import {AppServiceService} from '../../services/app-service.service';
import {FilterModel} from '../../services/filter-model';
import {ResponseModels} from '../../services/response-models';
import {ToastService} from '../../services/toast.service';
import {FunctinalityModel} from '../../services/functinality-model';


@Component({
    selector: 'app-recive-report',
    templateUrl: './recive-report.page.html',
    styleUrls: ['./recive-report.page.scss'],
})
export class ReciveReportPage implements OnInit {
    loading = false ;

    getTodayDate = new Date();
    dataFromModels: FunctinalityModel[] = [];
    data = {
        dateFrom: this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        dateUntil:  this.getTodayDate.getFullYear() + '/' + (this.getTodayDate.getMonth() + 1) + '/' + this.getTodayDate.getDate() ,
        organization: null ,
        organizationUnit: null ,
        systemUserId: null ,
        workShift: null
    };
    filterActive = false;
    result: ResponseModels ;
    unitName = '';
    input = {data: []};
    sumAll = 0 ;
    title = 'گزارش دریافتی';
    constructor(private service: AppServiceService , private toast: ToastService) {
    }

    ngOnInit() {
        this.getData(this.data);
    }

    getData(dataModel: FilterModel){
        this.loading = true ;
        this.service.getCreditor(dataModel).subscribe(
            res => {
                if (res.status) {
                    this.result = res ;
                    console.log(this.result);
                    this.dataFromModels = this.result.data ;
                    if (this.dataFromModels.length === 0){
                        this.toast.presentToast('اطلاعاتی ثبت نشده').then();
                    }
                    this.loading = false ;
                }
            },
            err => {
                this.loading = false ;
                this.toast.presentToast('خطای ارتباط با سرور').then();
            }
        );
    }

    dataFromModel(dataFromFilter: FilterModel) {
        this.getData(dataFromFilter);
    }

}
