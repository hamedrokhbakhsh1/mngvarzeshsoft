import { Injectable } from '@angular/core';
import {WorkshiftModel} from './workshift-model';
import {AppServiceService} from './app-service.service';
import {ResponseModels} from './response-models';
import {UserModel} from './user-model';
import {OrganizationModel} from './organization-model';
import {OrganizationUnitModel} from './organization-unit-model';
import {ToastService} from './toast.service';


@Injectable({
  providedIn: 'root'
})
export class FilterDataService {
  workShitData: WorkshiftModel[];
  organizationModel: OrganizationModel[];
  organizationUnitModel: OrganizationUnitModel[];
  userDataModel: UserModel[];
  result: ResponseModels ;
    workShifts = {data: []};
    userData = {data: []};
    organizations = { data: []};
    organizationUnits = { data: []};

    constructor(private service: AppServiceService , private toast: ToastService) {
    this.setDataFilter();

  }

  getWorkShiftData(){
    return this.workShifts ;
  }

  getUserData(){
      return this.userData ;
  }

  getOrganizationData(){
        return this.organizations;
  }

  getOrganizationUnitData(){
        return this.organizationUnits ;
  }


  setDataFilter(){

    this.service.getAllFilterAction().subscribe(
        res => {
          if (res.status) {
            this.result = res ;
            // console.log(this.result.data[0].systemUser);
            this.workShitData = this.result.data[0].workShift;
            for (const entry of this.workShitData){
                  this.workShifts.data.push({name: entry.ID, type: 'radio', label: entry.Name, value: entry});
              }
            this.userDataModel = this.result.data[0].systemUser;
            // console.log(this.userDataModel)
            for (const entry of this.userDataModel){
                  this.userData.data.push({name: entry.ID, type: 'radio', label: entry.DisplayName, value: entry});
            }
            this.organizationModel = this.result.data[0].organization;
            // console.log(this.organizationModel);
            for (const entry of this.organizationModel){
                  this.organizations.data.push({name: entry.ID, type: 'radio', label: entry.Title, value: entry});
            }
            this.organizationUnitModel = this.result.data[0].organizationUnit;
            // console.log(this.organizationUnitModel);
            for (const entry of this.organizationUnitModel){
                  this.organizationUnits.data.push({name: entry.ID, type: 'radio', label: entry.Name, value: entry});
              }
            // console.log(this.organizationUnits);
          }
        },
        err => {
            this.toast.presentToast('خطای ارتباط با سرور').then();
        }
    );
  }
}
