export interface FilterModel {
    dateFrom: string ;
    dateUntil: string ;
    organizationUnit: string;
    systemUserId: string ;
    workShift: string ;
    organization: string;
}
