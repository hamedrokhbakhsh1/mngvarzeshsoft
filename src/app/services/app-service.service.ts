import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseIpModel} from './response-ip-model';
import {HttpClient} from '@angular/common/http';
import {appconfig} from '../appconfig';
import {IpModel} from './ip-model';
import {ResponseModel} from './response-model';
import {ResponseModels} from './response-models';
import {FilterModel} from './filter-model';




@Injectable({
    providedIn: 'root'
})
export class AppServiceService{

    packageUrl: string = null;
    constructor(private http: HttpClient) {
    }

    getIp(): Observable<ResponseIpModel> {
        return this.http.get<ResponseIpModel>(`${appconfig.api}`);
    }


    authUser(data: any): Observable<any> {
        const url = 'https://cors-anywhere.herokuapp.com/' + localStorage.getItem('url');
        return this.http.post<any>(`${url}/login`, data);
    }

    async storeUrl(id) {
        try {

            const res = await this.getIp().toPromise();
            const ipModel: IpModel = res.data[id];
            this.packageUrl = ipModel.ServerAddress;
            localStorage.setItem('url', this.packageUrl);
        } catch (e) {

        }
    }
    storeUserId(token , name){
        localStorage.setItem('token', token) ;
        localStorage.setItem('name' , name);
    }
    isLogin(): boolean {
        return localStorage.getItem('token') != null;
    }

    isHasUrl(): boolean {
        return localStorage.getItem('url') != null;
    }
    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('url');
        return true ;
    }


    getAllReport(data: FilterModel): Observable<any>{
        const url = 'https://cors-anywhere.herokuapp.com/' +  localStorage.getItem('url');
        return this.http.post<ResponseModels>(`${url}/get-all-report-status`, data);
    }
    getAllFilterAction(): Observable<ResponseModels>{
        // @ts-ignore
        const url = 'https://cors-anywhere.herokuapp.com/' + localStorage.getItem('url');
        return this.http.post<ResponseModels>(`${url}/get-all-data-filter` ,{});
    }


    getReportSituation(data: FilterModel): Observable<any>{
        const url = 'https://cors-anywhere.herokuapp.com/' + localStorage.getItem('url');
        return this.http.post<ResponseModels>(`${url}/get-pool-reception-limit`, data);
    }

    getDebtor(data: FilterModel): Observable<any>{
        const url =  'https://cors-anywhere.herokuapp.com/' + localStorage.getItem('url');
        return this.http.post<ResponseModels>(`${url}/get-creditor-amounts-limit`, data);
    }
    getCreditor(data: FilterModel): Observable<any>{
        const url = 'https://cors-anywhere.herokuapp.com/' +  localStorage.getItem('url');
        return this.http.post<ResponseModels>(`${url}/get-debtor-amounts-limit`, data);
    }


}
