export interface FunctinalityModel {
    OperationID?: string ;
    OperationCount?: number ;
    OperationName?: string ;
    OperationTitle?: string ;
    TotalAmount?: number;
    OrganizationUnitID?: string;
    CreatorUser?: string ;
    WorkShift?: string ;
    OrganizationID?: string ;
}
