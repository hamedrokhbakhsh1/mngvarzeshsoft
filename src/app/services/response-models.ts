export interface ResponseModels {
    status: boolean ;
    errorMessage?: string ;
    data?: any;
}
