import { NgModule } from '@angular/core';
import {FilterActionComponent} from '../pages/filter-action/filter-action.component';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {HeaderComponent} from '../pages/header/header.component';


@NgModule({
    imports: [CommonModule, IonicModule],
  declarations: [ FilterActionComponent , HeaderComponent] ,
  exports: [FilterActionComponent , HeaderComponent]
})
export class FilterModule { }
